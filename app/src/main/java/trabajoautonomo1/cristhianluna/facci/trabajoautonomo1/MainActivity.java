package trabajoautonomo1.cristhianluna.facci.trabajoautonomo1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText caja1, caja2, caja3, caja4;
    Button calcularFarenheit, calcularCelcius;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        caja1=(EditText)findViewById(R.id.editTextFarenheit);
        caja2=(EditText)findViewById(R.id.editTextResultado);
        calcularFarenheit=(Button)findViewById(R.id.botonCalcular);

        caja3=(EditText)findViewById(R.id.editTextCelcius);
        caja4=(EditText)findViewById(R.id.editTextResultado2);
        calcularCelcius=(Button)findViewById(R.id.botonCalcular2);


        calcularCelcius.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalcularCelcius();

            }
        });


        calcularFarenheit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalcularFarenheit();

            }
        });


    }

    public void CalcularCelcius(){
        double valorCelcius = Double.valueOf(caja3.getText().toString());

        double respuesta = (valorCelcius*1.8)+32;
        caja4.setText(respuesta+"°F");
    }


    public void CalcularFarenheit(){
        double valorFarenheit = Double.valueOf(caja1.getText().toString());

        double respuesta = (valorFarenheit-32)*0.56;
        caja2.setText(respuesta+"°C");
    }
}
